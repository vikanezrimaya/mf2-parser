# MF2 parser crate

This is a work in progress. It doesn't handle a lot, can't handle nested MF2 objects as properties properly (alliteration not intended),
doesn't have backwards compatibility with the old microformats spec, but it can already provide a fully typed representation of an MF2 object tree.

## TODO
 - [x] Parse a simple h-entry with content
 - [ ] Parse datetimes from ISO8601
 - [ ] Parse nested MF2 objects specified as object properties
   - [ ] Implement a way to properly recurse in such objects when parsing
 - [ ] Serialize everything into JSON for interoperation with other languages
   - [ ] Serialize simple entries
   - [ ] Serialize datetimes
   - [ ] Ensure Mf2::Struct objects nested as properties are properly serialized

## License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
