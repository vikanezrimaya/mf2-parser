use serde::{Serialize, Deserialize};
use serde_json::json;
use kuchiki::traits::*;
use std::collections::HashMap;
use http_types::Url;
use chrono::{DateTime,FixedOffset};

#[derive(Debug)]
pub struct Mf2Error {}
impl std::error::Error for Mf2Error {}
impl std::fmt::Display for Mf2Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "microformats2 deserialization error")?;
        Ok(())
    }
}
type Result<T> = std::result::Result<T, Mf2Error>;

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct Mf2Struct {
    #[serde(rename = "type")]
    obj_type: Vec<String>,
    properties: HashMap<String, Vec<Mf2>>,
    pub children: Vec<Mf2Struct>
}
impl Mf2Struct {
    pub fn new(types: Vec<String>) -> Self {
        Self {
            obj_type: types,
            properties: HashMap::new(),
            children: vec![]
        }
    }
    pub fn types(&self) -> &Vec<String> {
        &self.obj_type
    }
    pub fn prop(&self, name: &str) -> Vec<&Mf2> {
        self.properties.get(name).map(|v| v.iter().collect::<Vec<&Mf2>>()).unwrap_or_else(|| vec![])
    }
    pub fn insert_property(&mut self, name: &str, value: Mf2) {
        self.properties.entry(name.to_string()).or_insert(vec![]).push(value);
    }
    pub fn remove_property(&mut self, name: &str, value: Mf2) {
        if let Some(prop) = self.properties.get_mut(name) {
            if let Some(idx) = prop.iter().position(|v| v == &value) {
                prop.remove(idx);
            }
        }
    }
}
#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct Mf2RichMarkup {
    pub html: String,
    pub value: String
}
#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub enum Mf2 {
    /* h-  */ Struct(Mf2Struct),
    /* p-  */ Plain(String),
    /* dt- */ DateTime(DateTime<FixedOffset>),
    /* e-  */ RichMarkup(Mf2RichMarkup),
    /* u-  */ Url(Url)
}
impl Mf2 {
    pub fn as_struct(&self) -> Option<&Mf2Struct> {
        if let Mf2::Struct(s) = self { Some(s) } else { None }
    }
    pub fn as_plain(&self) -> Option<&str> {
        if let Mf2::Plain(s) = self { Some(s) } else { None }
    }
    pub fn as_datetime(&self) -> Option<&DateTime<FixedOffset>> {
        if let Mf2::DateTime(s) = self { Some(s) } else { None }
    }
    pub fn as_rich_markup(&self) -> Option<&Mf2RichMarkup> {
        if let Mf2::RichMarkup(s) = self { Some(s) } else { None }
    }
    pub fn as_url(&self) -> Option<&Url> {
        if let Mf2::Url(s) = self { Some(s) } else { None }
    }
}

#[derive(Serialize, Clone)]
pub struct Mf2Data {
    pub items: Vec<Mf2Struct>
}
impl Mf2Data {
    pub fn len(&self) -> usize {
        self.items.len()
    }
    pub fn get_first_of_type(&self, htype: &str) -> Option<&Mf2Struct> {
        self.items.iter().find(|h| h.types().iter().any(|t| t == htype))
    }
}
impl std::ops::Index<usize> for Mf2Data {
    type Output = Mf2Struct;
    fn index(&self, idx: usize) -> &Self::Output {
        self.items.index(idx)
    }

}
impl Default for Mf2Data {
    fn default() -> Self {
        Self {
            items: vec![]
        }
    }
}

fn parse_struct(class: Vec<String>, _base_url: &str, items: &mut Vec<Mf2Struct>, lvl: usize) {
    let value = Mf2Struct::new(class.iter().filter(|h| h.starts_with("h-")).map(|t| t.to_string()).collect());
    // TODO: Handle inner MF2 objects marked up as properties
    // such as `.u-author.h-card` that need to be inserted in
    // a specific property as a nested MF2-JSON object
    let mut itemlist = &mut *items;
    let additional_tags = class.iter()
        .filter(|h| h.starts_with("e-") || h.starts_with("u-") || h.starts_with("dt-") || h.starts_with("p-"))
        .collect::<Vec<_>>();
    let mut i = lvl;
    if additional_tags.is_empty() {
        // I'm pretty sure this will not handle multiple levels of nested structs inside properties. We need a better approach here.
        // Maybe populate an h-entry's properties before inserting it into the object tree?
        // but how?
        while i > 1 {
            itemlist = &mut itemlist.last_mut().unwrap().children;
            i -= 1
        }
        itemlist.push(value);
    } else {
        while i > 2 {
            itemlist = &mut itemlist.last_mut().unwrap().children;
            i -= 1
        }
        if let Some(item) = itemlist.last_mut() {
            for prop in additional_tags {
                let prop = prop.strip_prefix("e-")
                    .unwrap_or_else(|| prop.strip_prefix("dt-")
                        .unwrap_or_else(|| prop.strip_prefix("p-")
                            .unwrap_or_else(|| prop.strip_prefix("u-")
                                .unwrap_or(""))));
                if !prop.is_empty() {
                    item.insert_property(prop, Mf2::Struct(value.clone()))
                }
            }
        }
    }
}

fn parse_html(html: kuchiki::NodeRef, class: Vec<String>, _base_url: &str, items: &mut Vec<Mf2Struct>, lvl: usize) {
    let mut i = lvl;
    let mut itemlist = &mut *items;
    while i > 2 {
        itemlist = &mut itemlist.last_mut().unwrap().children;
        i -= 1
    }
    if let Some(item) = itemlist.last_mut() {
        for prop in class.iter().filter(|h| h.starts_with("e-")) {
            let prop = prop.strip_prefix("e-").unwrap();
            item.insert_property(prop, Mf2::RichMarkup(Mf2RichMarkup {
                html: html.children().map(|c| c.to_string()).fold(String::new(), |a, b| a + &b),
                value: "".to_string() // TODO: get all text nodes
            }));
        }
    }
}

fn _parse_mf2(html: kuchiki::NodeRef, base_url: &str, mut items: &mut Vec<Mf2Struct>, mut lvl: usize) {
    if let Some(element) = html.clone().as_element() {
        let borrowed_attrs = element.attributes.borrow();
        let class = borrowed_attrs
            .get("class")
            .unwrap_or("")
            // turns out this is an iterator. Cool!
            .split_ascii_whitespace()
            .map(|v| v.to_string()).collect::<Vec<_>>();
        #[cfg(test)]
        println!("{}, {:?}", element.name.local, class);
        if class.iter().any(|h| h.starts_with("h-")) {
            // We found an MF2 object. Yay!
            parse_struct(class, base_url, items, lvl);
            lvl += 1
        } else if class.iter().any(|h| h.starts_with("e-")) {
            // We found a rich markup object. Shiny!
            parse_html(html.clone(), class, base_url, items, lvl);
        } else if class.iter().any(|h| h.starts_with("p-")) {
            // We found some plain text. Maybe it's a title? Who knows!
            todo!()
        } else if class.iter().any(|h| h.starts_with("dt-")) {
            // We found a date and time. Let's see if it's valid?
            todo!()
        } else if class.iter().any(|h| h.starts_with("u-")) {
            // We found a link. Let's resolve it.
            todo!()
        }
    }
    // Just search in the children for something interesting
    for child in html.children() {
        _parse_mf2(child.clone(), base_url, &mut items, lvl);
    }
}

pub fn parse_mf2(html: &str, base_url: &str) -> Result<Mf2Data> {
    let document = kuchiki::parse_html().one(html);
    let root = document.clone();
    let mut result = vec![];
    _parse_mf2(document, base_url, &mut result, 0);
    drop(root);
    Ok(Mf2Data { items: result })
}

#[cfg(test)]
mod tests {
    use super::{parse_mf2};
    use serde_json::json;

    #[test]
    fn simple_page() {
        let html = r#"<html>
          <head>
            <title>Vika's test page</title>
          </head>
          <body>
            <article class="h-entry">
              <main class="e-content">
                <p>This is cool stuff!</p>
              </main>
            </article>
          </body>
        </html>"#;
        let json = json!({
            "type": ["h-entry"],
            "properties": {
                "content": [{
                    "html": "<p>This is cool stuff!</p>"
                }]
            }
        });
        let parsed = parse_mf2(html, "https://fireburn.ru/posts/mf2").unwrap();
        assert_eq!(parsed.len(), 1);
        println!("{}", serde_json::to_string_pretty(&parsed[0]).unwrap());
        assert_eq!(parsed[0].types(), &json["type"].as_array().unwrap().iter().map(|v| v.as_str().unwrap().to_string()).collect::<Vec<String>>());
        assert_eq!(parsed[0].prop("content")[0].as_rich_markup().unwrap().html.trim(),
                   json["properties"]["content"][0]["html"].as_str().unwrap().trim());
    }
}
