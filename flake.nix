{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    rust = {
      type = "github";
      owner = "oxalica";
      repo = "rust-overlay";
      ref = "master";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    naersk = {
      type = "github";
      owner = "nmattia";
      repo = "naersk";
      ref = "master";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = { self, nixpkgs, rust, flake-utils, naersk }: let
    supportedSystems = ["aarch64-linux" "x86_64-linux"];
    forAllSystems = f: flake-utils.lib.eachSystem supportedSystems f;
  in forAllSystems (system: let
    pkgs = import nixpkgs {
      localSystem.system = system;
      overlays = [ rust.overlay ];
    };
    rust-bin = pkgs.rust-bin.stable.latest;
  in {
    devShell = pkgs.mkShell {
      name = "rust-dev-shell";
      #buildInputs = with pkgs; [ openssl ];
      nativeBuildInputs = with pkgs; [
        pkg-config lld
        rust-bin.default
        rust-bin.rls
      ];
    };
  });
}
